FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/sdk:8.0 AS build-stage
ARG TARGETARCH

RUN curl -fsSL https://deb.nodesource.com/setup_21.x | bash - &&\
apt-get install -y nodejs

# copy csproj and restore as distinct layers
WORKDIR /source
COPY DotnetTemplate.Web/*.csproj .

# copy and publish app and libraries
COPY DotnetTemplate.Web/. .
RUN dotnet publish -a $TARGETARCH -o /app


# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:8.0
EXPOSE 8080
WORKDIR /app
# copy the executable published on line 15 to WORKDIR
COPY --from=build-stage /app .
USER $APP_UID
ENTRYPOINT ["./DotnetTemplate.Web"]
